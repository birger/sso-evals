Keycloak evaluation
===================


[Keycloak](http://www.keycloak.org/) is an access management solution developed
by redhat.  It is distributed under the apache license, version 2.0.
Development of keycloak began in 2013 and since then there have been 122
releases. It is now at version 4.0, which was released on on June 13.
Keycloak is written in Java and needs Java version 8 or newer. It is used by
redhat as its main single sign on solution on https://sso.redhat.com
There is _no_ Debian package for keycloak and not even a RFP.

# Installation

```
apt install jre-default
wget https://downloads.jboss.org/keycloak/4.0.0.Final/keycloak-4.0.0.Final.tar.gz
tar xzvf keycloak-4.0.0.Final.tar.gz
mv keycloak-4.0.0.Final /opt
ln -s /opt/keycloak-4.0.0.Final /opt/keycloak
/opt/keycloak/bin/standalone.sh
```

Keycloak listens on 8080 and 8443 on localhost by default. It is probably easier
to install `nginx` and proxy the connection to keycloak than to fiddle with the
java settings. The nginx configuration file would look something like this:

Define an upstream directive that points to the keycloak service:
```
upstream keycloak_server {
   server localhost:8080;
}
```

Point `/auth` to the keycloak service somewhere in the `server` directive:
```
        location /auth {
          proxy_pass http://keycloak_server;

          proxy_http_version 1.1;

          proxy_set_header Host               $host;
          proxy_set_header X-Real-IP          $remote_addr;
          proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for;
          proxy_set_header X-Forwarded-Proto  $scheme;
        }
```

# Basic Configuration

On first access keycloak asks to create an useraccount. This account will be
used for any further configuration. Keycloak has a very modern and tidy
interface.

![Screenshot of the landing page](01-Keycloak-01.png)

To use accounts from LDAP one has to add a provider in the `User Federation`
tab. The settings are pretty self explanatory and depend on your setup. These
are the values i chose for the test setup:
* Console Display Name: ldaptest
* Import Users: OFF
* Edit Mode: READ_ONLY
* Vendor: Other
* Username LDAP attribute: uid
* RDN LDAP attribute: uid (default)
* UUID LDAP attribute: entryUUID (default)
* User Object Classes: inetOrdPerson, organizationalPerson (default)
* Connection URL: ldap://my.ldap.server.tld
* Users DN: ou=people,dc=debian,dc=org
* Authentication Type: none

If everything is setup, it should be possible to login to the account
management on https://your.keycloak.tld/auth/realms/master/account. There it is
possible to manage the profile. TODO: this should somehow be locked donw!

# OAuth2

To use Keycloak as an OAuth2 provider, one has to first create a new `Client`.
Then you have to fill out basic settings:

* Client ID: foobar
* Client Protocol: openid-connect
* Root URL: https://your.application.tld/

The following settings were used for a test setup with a test oauth flask
application.

* Access Type: confidential
* Authorization Enabled: ON

After you have saved the Client, you can go to the `Credentials` tab and copy
the client secret, which you have to enter in your application.
In your OAuth2 client application, you have to use the following settings:
* client_id = your Client ID
* client_secret = The secret from the Credentials tab
* authorization_base_url = 'https://your.keycloak.tld/auth/realms/master/protocol/openid-connect/auth'
* userinfo_url = 'https://your.keycloak.tld/auth/realms/master/protocol/openid-connect/userinfo'
* token_url = 'https://your.keycloak.tld/auth/realms/master/protocol/openid-connect/token'
* redirect_uri = 'http://your.application.tld/callback'

# Further investigations
* which attributes can be communcated to the client (sshkey)
* is it possible to disable profile editing
* what about multiple user federation providers? (they have priority, so it
looks good)

# Overall
No debian package is definitly a big drawback.
Java is also a big drawback...
Redhat as a company backing the product is an advantage in terms of security
and care. The interface is very clean and the documentation is detailed and
easy to comprehend. After an initial orientation phase, there were not that
many knobs to push to get it to work.

# Addendum

Gitlab configuration

```
gitlab_rails['omniauth_enabled'] = true
gitlab_rails['omniauth_allow_single_sign_on'] = true
gitlab_rails['omniauth_block_auto_created_users'] = false
gitlab_rails['omniauth_providers'] = [
  {
    'name' => 'oauth2_generic',
    'app_id' => 'client-id',
    'app_secret' => 'client-secret',
    'args' => {
      client_options: {
        'site' => 'http://auth.example.org', # including port if necessary
        'authorize_url' => '/auth/realms/master/protocol/openid-connect/auth',
        'user_info_url' => '/auth/realms/master/protocol/openid-connect/userinfo',
        'token_url' => '/auth/realms/master/protocol/openid-connect/token'
      },
      user_response_structure: {
        attributes: { email:'email', first_name:'name', last_name:'family_name', name:'name', nickname:'preferred_username' },
        id_path: 'preferred_username'
      },
      name: 'keycloak', # display name for this strategy
      strategy_class: "OmniAuth::Strategies::OAuth2Generic" # Devise-specific config option Gitlab uses to find renamed strategy
    }
  }
]
```

# Addendum 20180627
* It is possible to use multiple ldap backends. They can be configured using priorities.
* When using LDAP in read only mode, the user sees a profile and password editing form
after login, but on clicking 'Save' is informed that editing is not possible because the
account is readonly. That could be considered an UX problem.
* it is possibl to pass custom attributes like sshPublicKey to the client. One has to add
a mapper in the respective LDAP provider under `User Federation`; In the test, the mapper
type was `user-attribute-ldap-mapper`. In addition, the client scope which should contain
the attribute has to be modified. To add the attribute `sshPublicKeys` to the `profile`
scope, go to `Client scopes` -> `profile` -> `Mappers` and create a new mapper with mapper
type `User Attribute`; The `Claim JSON Type` is `String` and it is `Multivalued`
