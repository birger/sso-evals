Lemonldap-NG evaluation
=======================

[Lemonldap-NG](http://lemonldap-ng.org/) is a WebSSO solution that features
mechanisms for authentication, authorization and accounting. It is distributed
under the GPL. The first release that is mentioned in the changelog was in 2006.
The current stable release is version 1.9.17, there is a Debian package
in stable with version 1.9.7. There are already release candidates for version
2.0.  Lemonldap-NG is written in Perl and uses Apache/ModPerl or Nginx/FastCGI
for delivering the web content. There are a lot of organisations using
Lemonldap-NG, most of them in France.

# Installation

```
apt install lemonldap-ng
```

Lemonldap-NG (from now on LLNG) ships three different virtualhost
configurations for the webserver- a manager, a portal and a handler
virtualhost. The manager ist the webinterface that is used for administering
LLNG, the portal is the frontend for logging in and authenticating users.
The handler is used for protecting web applications.

# Basic Configuration

There is a main configuration file in /etc/lemonldap-ng/lemonldap-ng.ini. The
probably most important setting in this file is there configuration storage
backend. The default is 'file', but LLNG can also store configuration in SQL
or LDAP.

The default login for the manager is 'dwho' with password 'dwho'.

![Screenshot of the landing page](02-LLNG-01.png)

The interface makes intensive use of Javascript and the menu is sometimes
hard to work with. The first thing to disable is the default 'configuration
reload' setting, because otherwise one gets a warning message everytime the
configuration is saved, that the remote configuration could not be saved.
This is disabled in General Parameters -> Configuration reload. By default,
the configuration is stored in a file in `/var/lib/lemonldap-ng/conf/`. Every
configuration change creates one new file with incrementing numbers in the
filename (lmConf-1.js, lmConf-2.js, ... where lmConf-1.js is the initial
configuration, installed by the Debian package).

To authenticate against LDAP, the Authentication Module in General Parameters
-> Authentication Parameters has to be set to LDAP. Then one can set the
relevant settings in General Parameters -> Authentication Parameters -> LDAP
parameters -> Connection- its basically only Server host, Server port and
Users search base. The settings can then be tested by logging in on the portal
website. Another important setting is the `Exported variables`- those are
attributes that LLNG can export to a client.

To use OAuth2, one has to enable General Parameters -> Issuer Modules -> OpenID
Connect. Also the rewrite rules (for the path `^/oauth2/` in the webserver
configuration file have to be active. Another important configuration setting
in the apache vhost settings for the portal is `CGIPassAuth On` (this
information came via support request on the LLNG mailinglist. I have not
found any reference to this in the documentation for the stable LLNG version).
Then one has to create an *OpenID Connect Relying Party*. The most important
settings here are the client id and the client secret under _OpenID Connect
Relying Parties_ -> _nameofyourparty_ -> _Options_ -> _Authentication_; One also
has to set the _Redirection address_. Also one has to configure the Relying
Party under _Exported attributes_: to use defined OAuth2 attributes, one has
to configure:
* email -> mail
* family\_name -> sn
* name -> cn
* preferred\_username -> uid

A test setup with a flask oauth2 application did work and the userinfo
attributes were transmitted correctly. Another test with authenticating
gitlab against LLNG worked.

# Further investigation
* which attributes are communicated to the client (sshkey)
* what parts of the portal can be disabled
* according to the documentation, multiple ldap backend should be possible

# Overall

The user interface is not very modern and it sometimes is hard to find the
right spot where a configuration setting has to be changed. In addition
the documentation seems to miss some bits. Also it seems that LLNG is a
very complex piece of software, with a lot of parts. But when questions where
asked on the mailinglist, they were answered very soon and they answers helped
to fix problems. Also one of the developers of LLNG is Debian maintainer and
might help out maintaining and setting up LLNG. Also version 2 seems not to
be far away, that might make things easier.

# Addendum

Gitlab configuration for connection to LLNG:
```
gitlab_rails['omniauth_enabled'] = true
gitlab_rails['omniauth_allow_single_sign_on'] = true
gitlab_rails['omniauth_block_auto_created_users'] = false
gitlab_rails['omniauth_providers'] = [
  {
    'name' => 'oauth2_generic',
    'app_id' => 'client-id',
    'app_secret' => 'client-secret',
    'args' => {
      client_options: {
        'site' => 'http://auth.example.com', # including port if necessary
        'authorize_url' => '/oauth2/authorize?scope=openid+email+profile',
        'user_info_url' => '/oauth2/userinfo',
        'token_url' => '/oauth2/token'
      },
      user_response_structure: {
        attributes: { email:'email', first_name:'name', last_name:'family_name', name:'name', nickname:'preferred_username' },
        id_path: 'preferred_username'
      },
      name: 'lemonldap', # display name for this strategy
      strategy_class: "OmniAuth::Strategies::OAuth2Generic" # Devise-specific config option Gitlab uses to find renamed strategy
    }
  }
]
```

# Addendum 20180704
* It is possible to use multiple ldap backends; They have to be configured using the
`/etc/lemonldap-ng/lemonldap-ng.ini` configuration file. In the `[portal]` section
on has to add a `multi` configuration, like:
```
[portal]
multi = { \
  'LDAP#slapd1'=>{ \
    'ldapServer'=>'xxx.xxx.xxx.xxx', \
    'ldapBase' => 'ou=people,dc=debian,dc=org', \
    'LDAPFilter'=>'(uid=$user)'}, \
  'LDAP#slapd2'=>{ \
    'ldapServer'=>'yyy.yyy.yyy.yyy', \
    'ldapBase' => 'ou=people,dc=something,dc=else,dc=at', \
    'LDAPFilter'=>'(uid=$user)'}}
```
Then one has to enable the `Multiple` authentication and users module in the manager
* According to the documentation it is possible to pass custom attributes to the
client. I wasn't able to reproduce the behaviour.
